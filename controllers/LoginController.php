<?php
namespace IUTCTF\Controller;

class LoginController extends Controller
{
	/**
	* Displays the login page
	*/
	public function seePage(): void
	{
		parent::view('general-login');
	}

	/**
	* Check the login and password. Session creation
	*/
	public function postLogin(): void
	{
		$username = $_POST['login'] ?? NULL;
		$password = $_POST['password'] ?? NULL;
		if($username && $password) {
			if($username = 'admin' && password_verify($password, '$2y$10$XwiRCxWT54mNV6WygRhUbOLcMFc7Jp5YMMlRfmh8ZToZiY7txE/g.')) {
				$_SESSION['logged'] = true;
			}
		}

		parent::redirect('/');
	}

	/**
	* Log out and session destruction
	*/
	public function signout(): void
	{
		session_destroy();
		parent::redirect('/');
	}
}