<?php
namespace IUTCTF;
use IUTCTF\Router\Router;

spl_autoload_register(function($name) {
	$exploded = explode('\\', $name);
	$subdir = '';
	switch($exploded[1]) {
		case 'Router':
			$subdir = 'router/';
			break;
		case 'Controller':
			$subdir = 'controllers/';
			break;
		case 'Models':
			$subdir = 'models/';
			if ($exploded[2] === 'Entities') {
    		    $subdir = $subdir . 'entities/';
		    }
			break;
	}
	
    require '../'. $subdir . end($exploded) .'.php';
});

Router::init();

// ROUTES GENERALES

Router::setDefault('Login', ['logged' => false]);
Router::setDefault('NotFound', ['logged' => true]);

// MISE EN PLACE D'UNE SESSION PHP

session_start();

// TRAITEMENT DE LA ROUTE

Router::run();