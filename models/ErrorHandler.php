<?php
namespace IUTCTF\Models;

class ErrorHandler
{
	private $errors = [];
	private $values;

	public function __construct(array $values)
	{
		$this->values = $values;
	}

	public function getErrors(): array
	{
		return $this->errors;
	}

	public function addError(string $inputName, string $error): void
	{
		$this->errors[$inputName] = $error;
	}

	public function emptyInput(string $inputName): void
	{
		$this->addError($inputName, "Veuillez remplir le champ");
	}

	public function hasErrors(): bool
	{
		return !empty($this->errors);
	}

	public function getValues(): array
	{
		return $this->values;
	}

	public function setValue(string $inputName, string $value): void
	{
		if(array_key_exists($inputName, $this->values)) {
			$this->values[$inputName] = $value;
		}
	}

	public function verifyEmptyInputs(array $requiredInputs): void
	{
		foreach($requiredInputs as $inputName) {
			if(!isset($_POST[$inputName]) || (isset($_POST[$inputName]) && empty($_POST[$inputName]))) $this->emptyInput($inputName);
		}
	}
}