<?php
namespace IUTCTF\Router;

class Router
{
    private static $url;
    private static $routes;
    private static $defaultRoutes;

    public static function init(): void
    {
        self::$url = $_SERVER['REQUEST_URI'];
        self::$routes = [];
        self::$defaultRoutes = [];
    }

    public static function setDefault($callback, array $options = []): void
    {
        self::$defaultRoutes[] = new Route('', $callback, $options);
    }

    public static function get(string $path, $callback, array $options = []): Route
    {
        return self::add($path, $callback, $options, 'GET');
    }

    public static function post(string $path, $callback, array $options = []): Route
    {
        return self::add($path, $callback, $options, 'POST');
    }
    
    public static function view(string $path, string $view, array $options = []): Route
    {
        return self::add($path, function() use ($view) {
            require '../views/'. $view .'.php';
        }, $options, 'GET');
    }

    private static function add(string $path, $callback, array $options, string $method): Route
    {
        $route = new Route($path, $callback, $options);
        self::$routes[$method][] = $route;
        return $route;
    }

    public static function run()
    {
        if (!isset(self::$routes[$_SERVER['REQUEST_METHOD']])) {
            throw new RouterException('REQUEST_METHOD does not exist');
        }
        
        foreach (self::$routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if (self::isMatchingRoute($route, self::$url)) {
                return $route->call();
            }
        }
        
        // Check des routes par défaut
        foreach(self::$defaultRoutes as $defaultRoute) {
            if($defaultRoute->verifyOptions()) {
                return $defaultRoute->call();
            }
        }

        return self::$defaultRoutes[0]->call();
    }
    
    private static function isMatchingRoute(Route $route, string $url): bool
    {
        $url = trim($url, '/');
        $path = preg_replace('#{([a-zA-Z0-9]+)*}#', '([^/]+)', $route->getPath());
        $regex = '#^'. $path. '$#i';
        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        array_shift($matches);
        $route->setMatches($matches);
        return $route->verifyOptions();
    }
}